import React, { Component } from 'react';
import { Tabs, TabList, TabPanel, Tab } from 'react-re-super-tabs'
import logo from './assets/LOGO-ACID-white.png';
import './App.css';
import Months from "./tabs/months";
import Hours from "./tabs/hours";
import CustomTab from "./tabs/customtab";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" onClick={this.handleTabClick} />
          <h1 className="App-title">Desafío Acid</h1>
        </header>
      <Tabs activeTab='hours'>
          <TabList>
            <Tab component={CustomTab} label='Por Hora' id='hours' />
            <Tab component={CustomTab} label='Por Mes' id='months'/>
          </TabList>
          <TabList>
              <TabPanel component={Hours} id='hours'/>
              <TabPanel component={Months} id='months' />
          </TabList>
      </Tabs>
      </div>
    );
  }

  handleTabClick = (event) => {
      alert('hola');
      console.log('clicked')
  }
}

export default App;
