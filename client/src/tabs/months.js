import React, {Component} from 'react'
import LineChart from '../linechart/linechart'
import ToolTip from '../linechart/tooltip';
import moment from  'moment';

class Months extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fetchingData: true,
            data: null,
            hoverLoc: null,
            activePoint: null
        };
    }

    render() {
        return (
            <div className="tab-view">
                <div className='row'>
                    <div className='popup'>
                        {this.state.hoverLoc ? <ToolTip hoverLoc={this.state.hoverLoc} activePoint={this.state.activePoint}/> : null}
                    </div>
                </div>
                <div className='row'>
                    <div className='chart'>
                        { !this.state.fetchingData ?
                            <LineChart data={this.state.data} onChartHover={ (a,b) => this.handleChartHover(a,b) }/>
                            : null }
                    </div>
                </div>
                <button className="fetch-btn" onClick={this.fetchData}>Fetch Data</button>
            </div>
        );
    }

    handleChartHover(hoverLoc, activePoint){
        this.setState({
            hoverLoc: hoverLoc,
            activePoint: activePoint
        })
    }

    fetchData = () => {
        this.setState({
            fetchingData: true
        });

        console.log('fetching data');
        fetch('/month')
            .then(results => {
                return results.json();
            })
            .then(data => {
                const sortedData = [];
                let filteredData = {};
                let count = 0;
                let dates = data['Time Series (Digital Currency Monthly)'];


                Object.keys(dates).reverse().forEach(function (key, index) {
                    sortedData.push({
                        d: moment(key).format('MMMM YYYY'),
                        p: parseFloat(dates[key]['4b. close (USD)']).toLocaleString('us-EN',{ style: 'currency', currency: 'USD' }),
                        x: count, //previous days
                        y: parseFloat(dates[key]['4b. close (USD)']) // numerical price
                    });
                    count++;
                });

                this.setState({
                    data : sortedData,
                    fetchingData: false
                })
            })
    }

    componentDidMount() {
        this.fetchData();
    }

    componentWillUnmount() {

    }
}
export default Months
