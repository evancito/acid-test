import React, {Component} from 'react'
import LineChart from '../linechart/linechart'
import ToolTip from '../linechart/tooltip';
import moment from  'moment';

class Hours extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fetchingData: true,
            data: null,
            hoverLoc: null,
            activePoint: null
        };
    }

    render() {
        return (
            <div className="tab-view">
                <div className='row'>
                    <div className='popup'>
                        {this.state.hoverLoc ? <ToolTip hoverLoc={this.state.hoverLoc} activePoint={this.state.activePoint}/> : null}
                    </div>
                </div>
                <div className='row'>
                    <div className='chart'>
                        { !this.state.fetchingData ?
                            <LineChart data={this.state.data} onChartHover={ (a,b) => this.handleChartHover(a,b) }/>
                            : null }
                    </div>
                </div>
                <button className="fetch-btn" onClick={this.fetchData}>Fetch Data</button>
            </div>
        );
    }

    handleChartHover(hoverLoc, activePoint){
        this.setState({
            hoverLoc: hoverLoc,
            activePoint: activePoint
        })
    }

    fetchData = () => {
        this.setState({
            fetchingData: true
        });

        console.log('fetching data');
        fetch('/hour')
            .then(results => {
                return results.json();
             })
            .then(data => {
                const sortedData = [];
                let filteredData = {};
                let count = 0;
                let dates = data['Time Series (Digital Currency Intraday)'];

                Object.keys(dates).reverse().forEach(function(key,index) {
                    // key: the name of the object key
                    // index: the ordinal position of the key within the object
                    console.log('key: '+ key+'  index: '+index);

                    let  key2 = moment(key).format('YYYY-MM-DD HH:00:00');
                    console.log('new key: '+key2);

                    if (!filteredData[key2]) {
                        filteredData[key2] = dates[key];
                    }else {
                        if (key2 === key) {
                            filteredData[key2] = dates[key];
                        }
                    }
                });

                Object.keys(filteredData).forEach(function (key, index) {
                    sortedData.push({
                        d: moment(key).format('HH:mm')+' hrs.',
                        p: parseFloat(filteredData[key]['1b. price (USD)']).toLocaleString('us-EN',{ style: 'currency', currency: 'USD' }),
                        x: count, //previous days
                        y: parseFloat(filteredData[key]['1b. price (USD)']) // numerical price
                    });
                    count++;
                });

                this.setState({
                    data : sortedData,
                    fetchingData: false
                })
            })
    }

    componentDidMount() {
        this.fetchData();
    }

    componentWillUnmount() {

    }
}
export default Hours
