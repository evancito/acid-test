Run Project.

### Requirements
 
 Este proyecto está contruido utilizando NodeJS, Redis y Yarn, por lo que deberán estar 
 instalados de forma global. 
 
 
```sh
$ brew install yarn
```

```sh
$ brew install node
```
```sh
$ brew install redis
```




### Instalación de dependencias.

Este proyecto cuenta con 2 archivos package.json distintos, uno para el servidor y otro
para el cliente. por lo que deberás ejecutar 


```sh
$ npm install
```

```sh
$ cd client/  &&  npm install 
```


### Run Project 

Para hacer correr este proyecto, debes iniciar primero la base de datos Redis

```sh
$ redis-server
```

Luego, desde la raíz del proyecto, deberás ejecutar 


```sh
$ yarn dev
```
Lo cual iniciará el servidor Node en el puerto 8080 y la aplicación React en el puerto 3000.