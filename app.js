var redis = require('redis');
var http = require('http');
var https = require('https');

var redisClient = redis.createClient({host : 'localhost', port : 6379});

const API_KEY = 'E9W4MQMXJHNF8O4J';
const HOURS_KEY = 'hours';
const MONTHS_KEY = 'months'

redisClient.on('ready',function() {
    console.log("Redis is ready");
});

redisClient.on('error',function(err) {
    console.log("Error in Redis", err);
});


http.createServer((request, response) => {
    request.on('error', (err) => {
        console.error(err);
        response.statusCode = 400;
        response.end();
    });
    response.on('error', (err) => {
        console.error(err);
    });

    if (request.method === 'GET' && request.url === '/hour') {
        console.log('GET /hour');
        getDataPerHour(request, response);
        //request.pipe(response);
    }else if(request.method === 'GET' && request.url === '/month') {
        console.log('GET /month');
        getDataPerMonth(request, response);
        //request.pipe(response);
    }else {
        response.statusCode = 404;
        response.end();
    }
}).listen(8080);


function getDataPerHour(req, res) {
    var expireTime = 60 * 60   // 3600 seconds = 1 hour.

    redisClient.get(HOURS_KEY, (err, result) => {
        if (!result){
            console.log('No data, retrieving from https://www.alphavantage.co');
            https.get('https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_INTRADAY&symbol=BTC&market=EUR&apikey='+API_KEY,
                (resp) => {
                    let data = '';

                    // A chunk of data has been recieved.
                    resp.on('data', (chunk) => {
                        data += chunk;
                    });
                    resp.on('end', () => {
                        redisClient.setex(HOURS_KEY, expireTime, JSON.stringify(data));
                        res.end(data);
                        console.log('onEnd GET /hour');
                    });
                })
        }else {
            console.log('Exist Data, retrieving from redis');
            res.end(JSON.parse(result));
        }
    })
}

function getDataPerMonth(req, res) {
    var expireTime = 60 * 60 * 24;  // 86400 seconds = 24hrs.

    redisClient.get(MONTHS_KEY, (err, result) => {
        if (!result){
            console.log('No data, retrieving from https://www.alphavantage.co');
            https.get('https://www.alphavantage.co/query?function=DIGITAL_CURRENCY_MONTHLY&symbol=BTC&market=EUR&apikey='+API_KEY,
                (resp) => {
                    let data = '';
                    // A chunk of data has been recieved.
                    resp.on('data', (chunk) => {
                        data += chunk;
                    });
                    resp.on('end', () => {
                        redisClient.setex(MONTHS_KEY, expireTime ,JSON.stringify(data));
                        res.end(data);
                        console.log('onEnd GET /month');
                    });
                })
        }else {
            console.log('Exist Data, retrieving from redis');
            res.end(JSON.parse(result));
        }
    })
}